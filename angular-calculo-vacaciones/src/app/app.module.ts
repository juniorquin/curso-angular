import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PermisoVacacionesComponent } from './permiso-vacaciones/permiso-vacaciones.component';

@NgModule({
  declarations: [
    AppComponent,
    PermisoVacacionesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
