export class Permiso {
  restante: number;
  acumulador: number;
  diasTomados: number;
  aviso: number;
  constructor(public fechaInicio: string, public fechaFin) {
    this.restante = 0;
    this.acumulador = 0;
    this.diasTomados = 0;
    this.aviso = 0;
  }
  diferenciaFechas(): number {
    let diferencia = 0;
    const dtFechaInicio = new Date(Number(this.fechaInicio.split('-')[0]),
      Number(this.fechaInicio.split('-')[1]) - 1,
      Number(this.fechaInicio.split('-')[2]));
    const dtFechaFin = new Date(Number(this.fechaFin.split('-')[0]),
      Number(this.fechaFin.split('-')[1]) - 1,
      Number(this.fechaFin.split('-')[2]));
    while (dtFechaInicio <= dtFechaFin) {
      if (dtFechaInicio.getDay() !== 0 && dtFechaInicio.getDay() !== 6) {
        diferencia++;
      }
      dtFechaInicio.setDate(dtFechaInicio.getDate() + 1);
    }
    return diferencia;
  }
}
