export class Periodo {
  restante: number;
  estados: boolean[];
  constructor(public anio: number) {
    this.restante = 30;
    this.estados = [false, false, false];
  }
}
