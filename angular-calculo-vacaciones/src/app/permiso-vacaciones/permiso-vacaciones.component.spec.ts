import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermisoVacacionesComponent } from './permiso-vacaciones.component';

describe('PermisoVacacionesComponent', () => {
  let component: PermisoVacacionesComponent;
  let fixture: ComponentFixture<PermisoVacacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermisoVacacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermisoVacacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
