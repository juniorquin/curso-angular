import { Component, OnInit } from '@angular/core';
import { Permiso } from '../models/permiso.model';
import { Periodo } from '../models/periodo.model';

@Component({
  selector: 'app-permiso-vacaciones',
  templateUrl: './permiso-vacaciones.component.html',
  styleUrls: ['./permiso-vacaciones.component.css']
})
export class PermisoVacacionesComponent implements OnInit {
  cantidadDias: number;
  sobra: number;
  permisos: Permiso[];
  periodos: Periodo[];
  acumulados: number;
  mensajeAlerta: boolean;
  mensajeText: string;
  mensajeQuinceDias: string;
  constructor() {
    this.cantidadDias = 0;
    this.sobra = 0;
    this.permisos = [];
    this.periodos = [];
    this.acumulados = 0;
    this.mensajeAlerta = false;
    this.mensajeText = '';
    this.mensajeQuinceDias = '';
  }

  ngOnInit() {
  }
  guardar(fechaIni: string, fechaFin: string): boolean {
    console.log(fechaIni, fechaFin);
    if (fechaFin === '') {
      const fechaActual = new Date();
      const dtFechaInicio = new Date(Number(fechaIni.split('-')[0]),
        Number(fechaIni.split('-')[1]) - 1,
        Number(fechaIni.split('-')[2]));
      this.cantidadDias = this.diferenciaMesesFecha(dtFechaInicio, fechaActual) * 2.5;
      for (let i = dtFechaInicio.getFullYear(); i <= fechaActual.getFullYear(); i++) {
        this.periodos.push(new Periodo(i));
      }
    } else {
      const dtFechaInicio = new Date(Number(fechaIni.split('-')[0]),
        Number(fechaIni.split('-')[1]) - 1,
        Number(fechaIni.split('-')[2]));
      const dtFechaFin = new Date(Number(fechaFin.split('-')[0]),
        Number(fechaFin.split('-')[1]) - 1,
        Number(fechaFin.split('-')[2]));
      this.cantidadDias = this.diferenciaMesesFecha(dtFechaInicio, dtFechaFin) * 2.5;
      for (let i = dtFechaInicio.getFullYear(); i <= dtFechaFin.getFullYear(); i++) {
        this.periodos.push(new Periodo(i));
      }
    }
    this.sobra = this.cantidadDias;
    return false;
  }
  guardarPermiso(fechaIni: string, fechaFin: string): boolean {
    console.log(fechaIni, fechaFin);
    const per = new Permiso(fechaIni, fechaFin);
    this.acumulados += per.diferenciaFechas();
    let x = 0;
    if (Math.trunc(this.acumulados / 5) > 0) {
      x = Math.trunc(this.acumulados / 5);
      this.acumulados -= 5 * x;
    }
    this.sobra -= per.diferenciaFechas() + 2 * x;
    let m = 0;
    this.mensajeAlerta = false;
    this.mensajeQuinceDias = '';
    for (const periodo of this.periodos) {
      if (periodo.restante !== 0) {
        // if (periodo.estados[2] || (periodo.estados[0] && periodo.estados[1])) { }
        if (m === 0) {
          if (periodo.restante - per.diferenciaFechas() - 2 * x >= 0) {
            switch (per.diferenciaFechas()) {
              case 7: {
                if (periodo.estados[0]) {
                  this.acumulados += 5 * x;
                  this.acumulados -= per.diferenciaFechas();
                  this.sobra += per.diferenciaFechas() + 2 * x;
                  this.mensajeAlerta = true;
                  this.mensajeText = `Ya hay un registro de 7 dias , le falta otro de 8 dias
                  para completar lo establecido para el periodo ${periodo.anio}
                  en caso ya haya registrado uno de 8 dias , usted ahora solo puede registrar
                  permisos menores a 7 dias`;
                  return false;
                }
                periodo.estados[0] = true;
                this.mensajeQuinceDias = periodo.estados[1] ? `Perfecto!!, ya registró 7 dias ,cumpliendo con el tramo establecido,
                ya no registre un permiso por mas de 7 días ,
                hasta que reduzcan a 0 el periodo'+ ${periodo.anio}-${periodo.anio + 1}` : `Perfecto!!, ya registró 7 días ahora
                le falta un registro de 8 días para cumplir con el tramo establecido o caso contrario solo registrar permisos
                menores a 7 días para el periodo ${periodo.anio}-${periodo.anio + 1}`;
                break;
              }
              case 8: {
                if (periodo.estados[1]) {
                  this.acumulados += 5 * x;
                  this.acumulados -= per.diferenciaFechas();
                  this.sobra += per.diferenciaFechas() + 2 * x;
                  this.mensajeAlerta = true;
                  this.mensajeText = `Ya hay un registro de 8 dias , le falta otro de 7 dias
                  para completar lo establecido para el periodo ${periodo.anio}
                  en caso haya registrado uno de 7 dias ,usted ahora solo puede registrar
                  permisos menores a 7 dias`;
                  return false;
                }
                periodo.estados[1] = true;
                this.mensajeQuinceDias = periodo.estados[0] ? `Perfecto!!, ya registró 8 dias ,cumpliendo con el tramo establecido,
                ya no registre un permiso por mas de 7 días ,
                hasta que reduzcan a 0 el periodo ${periodo.anio}-${periodo.anio + 1}` : `Perfecto!!, ya registró 8 dias ,le falta
                un registro de 7 días para cumplir con el tramo establecido o caso contrario solo registrar permisos menores a 7 días
                para el periodo ${periodo.anio}-${periodo.anio + 1}`;
                break;
              }
              case 15: {
                if (periodo.estados[2]) {
                  this.acumulados += 5 * x;
                  this.acumulados -= per.diferenciaFechas();
                  this.sobra += per.diferenciaFechas() + 2 * x;
                  this.mensajeAlerta = true;
                  this.mensajeText = `Ya hay un registro de 15 dias ,además
                  ya no registre un permiso por mas de 7 dias ,hasta que reduzcan a 0 el periodo ${periodo.anio}`;
                  return false;
                }
                periodo.estados[2] = true;
                this.mensajeQuinceDias = `Perfecto!!, ya registró 15 dias ,cumpliendo con el tramo establecido,
                ahora solo registre permisos menores a 7 días ,hasta que reduzcan a 0 el periodo
                ${periodo.anio}-${periodo.anio + 1}`;
                break;
              }
            }
            periodo.restante -= per.diferenciaFechas() + 2 * x;
            break;
          } else {
            switch (per.diferenciaFechas()) {
              case 7: {
                if (periodo.estados[0]) {
                  this.acumulados += 5 * x;
                  this.acumulados -= per.diferenciaFechas();
                  this.sobra += per.diferenciaFechas() + 2 * x;
                  this.mensajeAlerta = true;
                  this.mensajeText = `Ya hay un registro de 7 dias , le falta otro de 8 dias
                  para completar lo establecido para el periodo ${periodo.anio}
                  en caso ya haya registrado uno de 8 dias , usted ahora solo puede registrar
                  permisos menores a 7 dias`;
                  return false;
                }
                periodo.estados[0] = true;
                this.mensajeQuinceDias = periodo.estados[1] ? `Perfecto!!, ya registró 7 días ,cumpliendo con el tramo establecido
                ya no registre un permiso por mas de 7 días ,
                hasta que reduzcan a 0 el periodo'+ ${periodo.anio}-${periodo.anio + 1}` : `Perfecto!!, ya registró 7 días ahora
                le falta un registro de 8 días para cumplir con el tramo establecido o caso contrario solo registrar permisos
                menores a 7 días para el periodo ${periodo.anio}-${periodo.anio + 1}`;
                break;
              }
              case 8: {
                if (periodo.estados[1]) {
                  this.acumulados += 5 * x;
                  this.acumulados -= per.diferenciaFechas();
                  this.sobra += per.diferenciaFechas() + 2 * x;
                  this.mensajeAlerta = true;
                  this.mensajeText = `Ya hay un registro de 8 dias , le falta otro de 7 dias
                  para completar lo establecido para el periodo ${periodo.anio}
                  en caso haya registrado uno de 7 dias ,usted ahora solo puede registrar
                  permisos menores a 7 dias`;
                  return false;
                }
                periodo.estados[1] = true;
                this.mensajeQuinceDias = periodo.estados[0] ? `Perfecto!!, ya registró 8 días ,cumpliendo con el tramo establecido
                ya no registre un permiso por mas de 7 días ,
                hasta que reduzcan a 0 el periodo ${periodo.anio}-${periodo.anio + 1}` : `Perfecto!!, ya registró 8 dias ,le falta
                un registro de 7 días para cumplir con el tramo establecido o caso contrario solo registrar permisos menores a 7 días
                para el periodo ${periodo.anio}-${periodo.anio + 1}`;
                break;
              }
              case 15: {
                if (periodo.estados[2]) {
                  this.acumulados += 5 * x;
                  this.acumulados -= per.diferenciaFechas();
                  this.sobra += per.diferenciaFechas() + 2 * x;
                  this.mensajeAlerta = true;
                  this.mensajeText = `Ya hay un registro de 15 dias ,además
                  ya no registre un permiso por mas de 7 dias ,hasta que reduzcan a 0 el periodo ${periodo.anio}`;
                  return false;
                }
                periodo.estados[2] = true;
                this.mensajeQuinceDias = `Perfecto!!, ya registró 15 dias ,cumpliendo con el tramo establecido,
                ahora solo registre permisos menores a 7 días ,hasta que reduzcan a 0 el periodo
                ${periodo.anio}-${periodo.anio + 1}`;
                break;
              }
            }
            m = - periodo.restante + per.diferenciaFechas() + 2 * x;
            periodo.restante = 0;
          }
        } else {
          periodo.restante -= m;
          m = 0;
          break;
        }

      }
    }
    per.restante = this.sobra;
    per.diasTomados = per.diferenciaFechas() + 2 * x;
    per.aviso = x;
    per.acumulador = this.acumulados;
    this.permisos.push(per);

    return false;
  }
  diferenciaMesesFecha(d1: Date, d2: Date): number {
    let months: number;
    months = d2.getMonth() - d1.getMonth() +
      (12 * (d2.getFullYear() - d1.getFullYear()));
    if (d2.getDate() < d1.getDate()) { months--; }
    return months;
  }
}
