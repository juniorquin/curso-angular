import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Persona } from '../models/persona-dato.model';

@Component({
  selector: 'app-persona-datos',
  templateUrl: './persona-datos.component.html',
  styleUrls: ['./persona-datos.component.css']
})
export class PersonaDatosComponent implements OnInit {
  @Input() persona: Persona;
  @HostBinding('style.textTransform') textTransform = 'uppercase';
  constructor() { }

  ngOnInit() {
  }

}
