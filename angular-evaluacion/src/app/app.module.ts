import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListadoPersonasComponent } from './listado-personas/listado-personas.component';
import { PersonaDatosComponent } from './persona-datos/persona-datos.component';

@NgModule({
  declarations: [
    AppComponent,
    ListadoPersonasComponent,
    PersonaDatosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
