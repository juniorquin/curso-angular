import { Component, OnInit } from '@angular/core';
import { Persona } from '../models/persona-dato.model';

@Component({
  selector: 'app-listado-personas',
  templateUrl: './listado-personas.component.html',
  styleUrls: ['./listado-personas.component.css']
})
export class ListadoPersonasComponent implements OnInit {
  personas: Persona[];
  verAlerta: boolean;
  constructor() {
    this.personas = [];
    this.verAlerta = false;
  }

  ngOnInit() {
  }

  agregar(nombre: string , apellido: string): boolean {
    if (nombre.trim() === '' || apellido.trim() === '') {
      this.verAlerta = true;
      setTimeout( () => {
          this.verAlerta = false ;
      }, 3000);
    } else {
      this.verAlerta = false;
      this.personas.push(new Persona(nombre, apellido));
    }

    return false;
  }
}
