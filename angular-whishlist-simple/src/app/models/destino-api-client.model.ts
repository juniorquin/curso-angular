import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction, ResetVoteAction } from './destino-viaje-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  // destinos: DestinoViaje[];
  // current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) {
    // this.destinos = [];
  }
  add(d: DestinoViaje) {
    const headersC: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { nuevo: d.nombre }, { headers: headersC });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la bd');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
    // this.store.dispatch(new NuevoDestinoAction(d));
    // this.destinos.push(d);
  }
  /*
  getAll(): DestinoViaje[] {
    return this.destinos;
  }
  */

  getById(id: string): DestinoViaje {
    // return this.destinos.filter(d => { d.id.toString() === id })[0];
    return null;
  }

  elegir(destino: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(destino));
    /*
    this.destinos.forEach(d => {
      d.setSelected(false);
      // Object.assign({ selected: false }, d);

    });
    destino.setSelected(true);
    */
    // Object.assign({ selected: true }, destino);

    // this.current.next(destino);
  }
  /*
  subscribeOnChange(fn) {
    this.current.subscribe(fn);
  }
  */
  resetearVotos() {
    this.store.dispatch(new ResetVoteAction());
  }
}
