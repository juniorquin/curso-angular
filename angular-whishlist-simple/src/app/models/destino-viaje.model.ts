export class DestinoViaje {
  /*nombre: string;
  imagenUrl: string;
  constructor(nombre: string, imagenUrl: string) {
    this.nombre = nombre;
    this.imagenUrl = imagenUrl;
  }
  */
  public selected: boolean;
  public servicios: string[];
  constructor(public nombre: string, public imagenUrl: string, public id: Date, public votes: number = 0) {
    this.servicios = ['pileta', 'desayuno'];
    this.selected = false;
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  voteUp() {
    this.votes++;
  }
  voteDown() {
    this.votes--;
  }
}
