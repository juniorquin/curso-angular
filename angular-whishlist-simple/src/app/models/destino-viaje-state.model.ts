import { DestinoViaje } from './destino-viaje.model';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
// ESTADOS
export interface DestinosViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje;
}
/*
export const initializeDestinosViajesState = () => {
  return {
    items: [],
    loading: false,
    favorito: null
  };
};
*/
export function initializeDestinosViajesState() {
  return {
    items: [],
    loading: false,
    favorito: null,
  };
}
// ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  VOTE_UP = '[Destinos Viajes] Vote up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  RESET_VOTE = '[Destinos Viajes] Reset Vote',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}
export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) { }
}
export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) { }
}
export class VoteUpAction implements Action {
  type = DestinosViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje) { }
}
export class VoteDownAction implements Action {
  type = DestinosViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje) { }
}
export class ResetVoteAction implements Action {
  type = DestinosViajesActionTypes.RESET_VOTE;
  constructor() { }
}
export class InitMyDataAction implements Action {
  type = DestinosViajesActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]) { }
}
export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction |
  VoteUpAction | VoteDownAction | ResetVoteAction | InitMyDataAction;

// REDUCERS
export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesActions
): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return Object.assign({},
        state,
        {
          items: destinos.map(d => {
            console.log(new DestinoViaje(d, '', new Date()));
            return new DestinoViaje(d, '', new Date());
          })
        });
    }
    case DestinosViajesActionTypes.NUEVO_DESTINO: {
      return Object.assign({},
        state,
        {
          items: [...state.items, (action as NuevoDestinoAction).destino]
        });
    }
    case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
      /*
      state.items.forEach(x => {
        // Object.assign({ selected: false }, x);
        x.setSelected(false);
      });
      console.log(state.items);
      const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      // Object.assign({ selected: true }, fav);
      fav.setSelected(true)
      */
      return Object.assign({},
        state, {

        items: state.items.map((item) => {
          if (item.id === (action as ElegidoFavoritoAction).destino.id) {
            return Object.assign({}, item, { selected: true });
          }
          return Object.assign({}, item, { selected: false });
        }),
        favorito: (action as ElegidoFavoritoAction).destino
      });
    }
    case DestinosViajesActionTypes.VOTE_UP: {
      return Object.assign({},
        state, {

        items: state.items.map((item) => {
          if (item.id === (action as VoteUpAction).destino.id) {
            return Object.assign({}, item, { votes: item.votes + 1 });
          }
          return item;
        })
      });
    }
    case DestinosViajesActionTypes.VOTE_DOWN: {
      return Object.assign({},
        state, {

        items: state.items.map((item) => {
          if (item.id === (action as VoteDownAction).destino.id) {
            return Object.assign({}, item, { votes: item.votes - 1 });
          }
          return item;
        })
      });
    }
    case DestinosViajesActionTypes.RESET_VOTE: {
      return Object.assign({},
        state, {

        items: state.items.map((item) => {
          return Object.assign({}, item, { votes: 0 });
        })
      });
    }

  }
  return state;
}
//  EFFE
@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );
  constructor(private actions$: Actions) { }
}
