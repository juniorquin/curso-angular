import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() itemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  searchResults: string[];
  arrayFilter = ['barcelona', 'madrid', 'barranquilla'];

  minLongitud = 3;
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.itemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });
    /*
    this.fg.valueChanges.subscribe(form => {
      console.log('cambio el formulario', form);
    });
    */
  }

  ngOnInit() {
    const elemNombre = document.getElementById('nombre') as HTMLInputElement;
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => {
          return (e.target as HTMLInputElement).value;
        }),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => {
        // console.log(ajaxResponse);
        // this.searcResults = this.arrayFilter esto se usa para quitar switchmap  y no usar ajax
        this.searchResults = ajaxResponse.response;
      });
  }
  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url, new Date());
    this.itemAdded.emit(d);
    return false;
  }
  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const longitud = control.value.toString().trim().length;
    if (longitud > 0 && longitud < 5) {
      return { invalidNombre: true };
    }
    return null;
  }
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const longitud = control.value.toString().trim().length;
      if (longitud > 0 && longitud < minLong) {
        return { minLongNombre: true };
      }
      return null;
    };
  }
}
