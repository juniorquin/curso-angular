import { Component, OnInit, InjectionToken, Inject, forwardRef } from '@angular/core';
import { DestinosApiClient } from 'src/app/models/destino-api-client.model';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';
/*
export class DestinoApiClientViejo {
  getById(id: string): DestinoViaje {
    console.log('llamado por la clase vieja');
    return null;
  }
}
interface AppConfig {
  apiEndPoint: string;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi-api.com'
};
const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
export class DestinoApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }
  getById(id: string): DestinoViaje {
    console.log('llamado por la clase decorada');
    console.log('config ' + this.config.apiEndPoint);
    return null;
  }
}
*/
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    DestinosApiClient
    /*
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: DestinosApiClient, useClass: DestinoApiClientDecorated },
    { provide: DestinoApiClientViejo, useExisting: DestinosApiClient }
    */
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      id: 'countries',
      type: 'fill',
      source: 'world',
      layout: {},
      paint: {
        'fill-color': '#6F788A'
      }
    }]
  };
  // constructor(private destinosApiClient: DestinoApiClientViejo) { }
  constructor( private destinosApiClient: DestinosApiClient) { }

  ngOnInit() {
    this.destino = this.destinosApiClient.getById('adasd');
  }

}
