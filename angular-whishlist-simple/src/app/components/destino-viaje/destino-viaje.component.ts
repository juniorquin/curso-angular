import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { EmitterVisitorContext } from '@angular/compiler';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destino-viaje-state.model';
import { trigger, transition, state, style, animate } from '@angular/animations';
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'paleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'whiteSmoke'
      })),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ])
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() posicion: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit() {
  }
  ir() {
    this.clicked.emit(this.destino);
    return false;
  }
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
