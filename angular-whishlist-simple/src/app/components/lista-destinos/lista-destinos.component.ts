import { Component, OnInit, EventEmitter, Output, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destino-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino-viaje-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  updates: string[];
  all;
  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    // this.destinos = [];
    this.updates = [];
    /*
    this.store.subscribe(state => {
      console.log(state);
      this.destinos = state.destinos.items;
    });
    */
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('se ha elegido a' + d.nombre);
        }
      });
    /*
  this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
    if (d != null) {
      this.updates.push('se ha elegido a' + d.nombre);
    }
  });
  */
  }

  ngOnInit() {
  }

  agregado(destino: DestinoViaje) {
    this.destinosApiClient.add(destino);
    // this.store.dispatch(new NuevoDestinoAction(destino));
    // console.log(this.destinosApiClient);
  }
  elegido(destino: DestinoViaje) {
    this.destinosApiClient.elegir(destino);
    // this.store.dispatch(new ElegidoFavoritoAction(destino));
    // console.log(this.destinosApiClient);
  }
  resetVotos() {
    this.destinosApiClient.resetearVotos();
    return false;
  }
}
